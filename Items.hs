module Items (Item (..), Recipe (..), recipeOf, mapRecipe) where

-- An item which may be produced and/or used in a recipe.
data Item = 
  -- Logistics
    SteelChest
  | StorageTank
  | TransportBelt
  | MediumElectricPole
  | BigElectricPole
  | Substation
  | Pipe
  | PipeToGround
  | Pump
  | TrainStop
  | RailSignal
  | RailChainSignal
  | Locomotive
  | CargoWagon
  | FluidWagon
  | ActiveProviderChest
  | PassiveProviderChest
  | StorageChest
  | BufferChest
  | RequesterChest
  | Roboport
  | Lamp
  | RedWire
  | GreenWire
  | ArithmeticCombinator
  | DeciderCombinator
  | ConstantCombinator
  | PowerSwitch
  | ProgrammableSpeaker
  | StoneBrick
  | Concrete

  -- Production
  | RepairPack
  | Boiler
  | SteamEngine
  | NuclearReactor
  | HeatPipe
  | HeatExchanger
  | SteamTurbine
  | ElectricMiningDrill
  | OffshorePump
  | PumpJack
  | StoneFurnace
  | SteelFurnace
  | ElectricFurnace
  | AssemblyMachine1
  | AssemblyMachine2
  | AssemblyMachine3
  | OilRefinery
  | ChemicalPlant
  | Centrifuge
  | Lab
  | Beacon
  | SpeedModule

  -- Intermediate products
  | IronPlate
  | CopperPlate
  | SteelPlate
  | CopperCable
  | IronStick
  | IronGearWheel
  | ElectronicCircuit
  | AdvancedCircuit
  | EngineUnit

  -- Combat
  | FlamethrowerTurret
  | ArtilleryTurret
  | Radar

  deriving (Show, Eq, Ord)


-- Get the recipe which produces this item.
recipeOf :: Item -> Recipe
recipeOf item = case item of
  -- Logistic
  SteelChest -> Recipe [SteelPlate]
  StorageTank -> Recipe [IronPlate, SteelPlate]
  TransportBelt -> Recipe [IronGearWheel, IronPlate]
  MediumElectricPole -> Recipe [CopperPlate, IronStick, SteelPlate]
  BigElectricPole -> Recipe [CopperPlate, IronStick, SteelPlate]
  Substation -> Recipe [AdvancedCircuit, CopperPlate, SteelPlate]
  Pipe -> Recipe [IronPlate]
  PipeToGround -> Recipe [IronPlate, Pipe]
  Pump -> Recipe [EngineUnit, Pipe, SteelPlate]
  TrainStop -> Recipe [ElectronicCircuit, IronPlate, IronStick, SteelPlate]
  RailSignal -> Recipe [ElectronicCircuit, IronPlate]
  RailChainSignal -> Recipe [ElectronicCircuit, IronPlate]
  Locomotive -> Recipe [ElectronicCircuit, EngineUnit, SteelPlate]
  CargoWagon -> Recipe [IronGearWheel, IronPlate, SteelPlate]
  FluidWagon -> Recipe [IronGearWheel, Pipe, SteelPlate, StorageTank]
  ActiveProviderChest -> Recipe [AdvancedCircuit, ElectronicCircuit, SteelChest]
  PassiveProviderChest -> Recipe [AdvancedCircuit, ElectronicCircuit, SteelChest]
  StorageChest -> Recipe [AdvancedCircuit, ElectronicCircuit, SteelChest]
  BufferChest  -> Recipe [AdvancedCircuit, ElectronicCircuit, SteelChest]
  RequesterChest -> Recipe [AdvancedCircuit, ElectronicCircuit, SteelChest]
  Roboport -> Recipe [AdvancedCircuit, IronGearWheel, SteelPlate]
  Lamp -> Recipe [CopperCable, ElectronicCircuit, IronPlate]
  RedWire -> Recipe [CopperCable, ElectronicCircuit]
  GreenWire -> Recipe [CopperCable, ElectronicCircuit]
  ArithmeticCombinator -> Recipe [CopperCable, ElectronicCircuit]
  DeciderCombinator -> Recipe [CopperCable, ElectronicCircuit]
  ConstantCombinator -> Recipe [CopperCable, ElectronicCircuit]
  PowerSwitch -> Recipe [CopperCable, ElectronicCircuit, IronPlate]
  ProgrammableSpeaker -> Recipe [CopperCable, ElectronicCircuit, IronPlate, IronStick]

  -- Production
  RepairPack -> Recipe [ElectronicCircuit, IronGearWheel]
  Boiler -> Recipe [Pipe, StoneFurnace]
  SteamEngine -> Recipe [IronGearWheel, IronPlate, Pipe]
  NuclearReactor -> Recipe [AdvancedCircuit, Concrete, CopperPlate, SteelPlate]
  HeatPipe -> Recipe [CopperPlate, SteelPlate]
  HeatExchanger -> Recipe [CopperPlate, Pipe, SteelPlate]
  SteamTurbine -> Recipe [CopperPlate, IronGearWheel, Pipe]
  ElectricMiningDrill -> Recipe [ElectronicCircuit, IronGearWheel, IronPlate]
  OffshorePump -> Recipe [ElectronicCircuit, IronGearWheel, Pipe]
  PumpJack -> Recipe [ElectronicCircuit, IronGearWheel, Pipe, SteelPlate]
  SteelFurnace -> Recipe [SteelPlate, StoneBrick]
  ElectricFurnace -> Recipe [AdvancedCircuit, SteelPlate, StoneBrick]
  AssemblyMachine1 -> Recipe [ElectronicCircuit, IronGearWheel, IronPlate]
  AssemblyMachine2 -> Recipe [AssemblyMachine1, ElectronicCircuit, IronGearWheel, SteelPlate]
  AssemblyMachine3 -> Recipe [AssemblyMachine2, SpeedModule]
  OilRefinery -> Recipe [ElectronicCircuit, IronGearWheel, Pipe, SteelPlate, StoneBrick]
  ChemicalPlant -> Recipe [ElectronicCircuit, IronGearWheel, Pipe, SteelPlate]
  Centrifuge -> Recipe [AdvancedCircuit, Concrete, IronGearWheel, SteelPlate]
  Lab -> Recipe [ElectronicCircuit, IronGearWheel, TransportBelt]
  Beacon -> Recipe [AdvancedCircuit, CopperCable, ElectronicCircuit, SteelPlate]
  SpeedModule -> Recipe [AdvancedCircuit, ElectronicCircuit]

  -- Combat
  FlamethrowerTurret -> Recipe [EngineUnit, IronGearWheel, Pipe, SteelPlate]
  ArtilleryTurret -> Recipe [AdvancedCircuit, Concrete, IronGearWheel, SteelPlate]
  Radar -> Recipe [ElectronicCircuit, IronGearWheel, IronPlate]

-- A recipe.
newtype Recipe = Recipe [Item]
  deriving (Show)
  
mapRecipe :: ([Item] -> [Item]) -> Recipe -> Recipe
mapRecipe f (Recipe r) = Recipe $ f r
