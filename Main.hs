module Main (main) where

import Data.Maybe (maybeToList)
import Data.List (sort, foldl', foldr1, union)
-- import Data.List.Ordered (nubSort)
import Items
import Debug.Trace

-- A belt of material.  Models 2 alternating belts of different colors,
-- totalling 4 possible items.
newtype Belt = Belt [Item]
  deriving (Show)


-- The two belts surrounding a column of fabricators.
data ColumnBelts = ColumnBelts Belt Belt


-- A column of fabricators.
newtype FabColumn = FabColumn [Item]
  deriving (Show)
  

-- A solution to the problem.
data Solution = Solution [FabColumn] [Belt]
  deriving (Show)


-- Append an element at the end of a list (reverse `cons`).
snoc :: [a] -> a -> [a]
snoc [] x = [x]
snoc (head:tail) x = head:(snoc tail x)


-- A belt without any items on it.
emptyBelt :: Belt
emptyBelt = Belt []


-- Checks whether an item is already present on a belt.
inBelt :: Item -> Belt -> Bool
inBelt item (Belt xs) =
  item `elem` xs


-- Maximum number of different items on a belt.
itemsPerBelt :: Int
itemsPerBelt = 4


-- Add an item to a belt, if the belt isn't already saturated.
addToBelt :: Item -> Belt -> Maybe Belt
addToBelt item (Belt xs) = 
  if length xs >= itemsPerBelt
  then Nothing
  else Just (Belt (item:xs))
  

addToColumn :: Item -> FabColumn -> FabColumn
addToColumn item (FabColumn items) = FabColumn (item:items)


-- Apply a function to every column.
mapColumns :: (FabColumn -> ColumnBelts -> [(FabColumn, ColumnBelts)]) -> [FabColumn] -> [Belt] -> [Solution]
mapColumns f columns belts = mapColumns' f ([], columns) ([], belts)
  where
    mapColumns' f (prevColumns, column:restColumns) (prevBelts, l:r:restBelts) =
      map replaceColumn (f column (ColumnBelts l r))
        ++ mapColumns' f (prevColumns `snoc` column, restColumns) (prevBelts `snoc` l, r:restBelts)
      where
        replaceColumn (column', ColumnBelts l' r') =
          Solution (prevColumns ++ column':restColumns) (prevBelts ++ l':r':restBelts)
    mapColumns' _ _ _ = []


-- Add items to the given column's belts, such that the given recipe may be
-- produced in that column.
addRecipeToBelts :: Recipe -> ColumnBelts -> [ColumnBelts]
addRecipeToBelts (Recipe []) c = [c]
addRecipeToBelts (Recipe (x:xs)) (ColumnBelts l r) =
  if x `inBelt` l || x `inBelt` r
  then addRecipeToBelts (Recipe xs) (ColumnBelts l r)
  else toLeft ++ toRight
    where
      toLeft = concatMap (\l' -> addRecipeToBelts (Recipe xs) (ColumnBelts l' r)) $ addToBelt x l
      toRight = concatMap (\r' -> addRecipeToBelts (Recipe xs) (ColumnBelts l r')) $ addToBelt x r


-- Place the given item's fabricator in the given column, adding its recipe
-- to the belts if necessary.
placeFab :: Item -> FabColumn -> ColumnBelts -> [(FabColumn, ColumnBelts)]
placeFab item column =
  map (\belts' -> (foldl' (flip addToColumn) column items, belts')) 
    . addRecipeToBelts recipeWithoutItems
  where
    items = findFabsToReplace item
    combinedRecipe = combineRecipes $ map recipeOf items
    recipeWithoutItems = mapRecipe (filter (not . flip elem items)) combinedRecipe


findFabsToReplace :: Item -> [Item]
findFabsToReplace item = item : itemsToReplaceRecursive
  where
    (Recipe items') = recipeOf item
    itemsToReplace = filter (`elem` fabsToReplace) items'
    itemsToReplaceRecursive = concatMap findFabsToReplace itemsToReplace


combineRecipes :: [Recipe] -> Recipe
combineRecipes = foldr1 (\(Recipe l) (Recipe r) -> Recipe $ union l r)


-- data Constraint = Constraint
--   { pruneFabColumn :: FabColumn -> Bool
--   , pruneBelt :: Belt -> Bool
--   , prune }


-- Given `nColumns` columns of fabricator interleaved with `nColumns+1` double
-- belts, find all possible item allocation on these belts such that every
-- item in `fabs` can be produced in the store.
solve :: Int -> (FabColumn -> Bool) -> [Item] -> [Solution]
solve nColumns pruneFabColumn = solve' columns belts
  where
    belts = replicate (nColumns + 1) emptyBelt
    columns = replicate nColumns (FabColumn [])

    solve' :: [FabColumn] -> [Belt] -> [Item] -> [Solution]
    solve' columns belts [] = [Solution columns belts]
    solve' columns belts (fab:fabsRemaining) = 
      concatMap (\(Solution columns' belts') -> solve' columns' belts' fabsRemaining)
        $ mapColumns (\column belts -> filter (pruneFabColumn . fst) $ placeFab fab column belts) columns belts


fabsInColumn :: FabColumn -> Int
fabsInColumn (FabColumn fabs) = length fabs


atMostNFabsInColumn :: Int -> FabColumn -> Bool
atMostNFabsInColumn n = (<= n) . fabsInColumn


-- Compute the total number of items allocated on all belts.
beltOccupation :: [Belt] -> Int
beltOccupation [] = 0
beltOccupation ((Belt items):rest) = length items + beltOccupation rest


fabs = [MediumElectricPole, BigElectricPole, Substation,
  Pipe, PipeToGround, Pump, TrainStop, RailSignal, RailChainSignal,
  Locomotive, CargoWagon, FluidWagon, ActiveProviderChest,
  PassiveProviderChest, StorageChest, BufferChest, RequesterChest,
  Roboport, Lamp, RedWire, GreenWire, ArithmeticCombinator, DeciderCombinator,
  ConstantCombinator, PowerSwitch,
  ProgrammableSpeaker, RepairPack, SteamEngine, HeatPipe, HeatExchanger,
  SteamTurbine, ElectricMiningDrill, OffshorePump, PumpJack, ElectricFurnace,
  AssemblyMachine3, OilRefinery, ChemicalPlant, Centrifuge,
  Lab, Beacon, FlamethrowerTurret, ArtilleryTurret, Radar]


fabsToReplace = [AssemblyMachine1, AssemblyMachine2, SpeedModule, TransportBelt, StorageTank]


main :: IO ()
-- main = putStr $ show $ head $ filter ((<= 20) . beltOccupation . fst) $ solve 5 fabs
main = do
  let solution = head $ solve 9 (atMostNFabsInColumn 6) fabs
  let Solution columns belts = solution
  let fabsInSolution = concatMap (\(FabColumn item) -> item) columns
  print $ all (`elem` fabsInSolution) fabs
  print solution

