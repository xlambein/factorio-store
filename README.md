# Factorio Store Design


In Factorio, a common pattern for making common items that aren't required in
huge amounts is to build a _store_.  This allows the player to avoid waiting
for items to be made, and when construction bots have been researched, to make
the items available to them.

One way to build a store is to set up fabricators in a grid pattern, whose
smallest element is the following block:

```
║>FFF<║
║ FFF ║
║>FFF<║
║  v  ║  
║  X  ║
║     ║
```

where `║` is a vertical belt, `F` is a fabricator, `X` is a box, and `><v^`
are inserters.

This pattern allows to run up to 4 different materials down the two belts, or 8
if alternating yellow/red or red/blue belts are used.  The pattern is repeated
such that belts are shared between two columns.

Most items have raw materials in common with others, therefore, the design of
such a store lends itself to optimization, in order to ensure fabricators share
input belts as efficiently as possible.

The store can be modeled as follows:

- there are `n_belts` vertical belts and `n_cols = n_belts - 1` columns of
  fabricators
- a vertical belt has at most 4 different items on it
- a fabricator must be surrounded by belts that provide all the items it
  requires
- we want to minimize the numbers of columns/belts


